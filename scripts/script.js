let res = null
let xhr = new XMLHttpRequest()
const avatar = document.querySelector('.avatar')
const name = document.querySelector('.name')
const friends = document.getElementById('friends')
const button = document.querySelector('button')
const input = document.querySelector('input')
const replies = document.querySelector('.replies')

button.addEventListener('click', addReply)
input.addEventListener('keydown', (e) => {
  if (e.which == 13) addReply()
})

function addReply() {
  let text = input.value.trim()
  if (!text.length) return

  let reply = document.createElement('div')
  reply.classList.add('reply')
  reply.innerHTML = `<p>${text}</p>`
  replies.appendChild(reply)
  input.value = null
}

function drawPhotos(photos) {
  const gallery = document.getElementById('gallery')
  console.log('in drawPhoto()');
  photos.forEach(drawPhoto)

  function drawPhoto(el) {
    let photo = document.createElement('div'),
      img = document.createElement('img')

    photo.classList.add('photo')
    img.src = el.url

    photo.appendChild(img)
    gallery.appendChild(photo)
  }
}

function drawUser() {
  let profile = JSON.parse(xhr.responseText).results[0]
  let avatar = document.getElementsByClassName('avatar')[0],
    name = document.getElementsByClassName('name')[0],
    cell = document.getElementsByClassName('phone')[0],
    email = document.getElementsByClassName('email')[0]

  avatar.src = profile.picture.large
  name.textContent = profile.name.first + ' ' + profile.name.last
  cell.textContent = profile.cell
  email.textContent = profile.email
}

function drawFriends(users) {
  console.log(users);
  users.results.forEach(drawFriend)

  function drawFriend(el) {
    let friend = document.createElement('div')
    photo = document.createElement('img')

    friend.classList.add('friend')
    photo.src = el.picture.medium
    friend.appendChild(photo)
    friends.appendChild(friend)
  }
}

function get(url, callback) {
  xhr.onreadystatechange = (e) => {
    if (xhr.status == 200 && xhr.readyState == 4) {
      callback(JSON.parse(xhr.responseText))
    }
  }

  xhr.open('GET', url, false)
  xhr.send(null)
}

{
  get('/photos', drawPhotos)
  get('https://randomuser.me/api/?results=15', drawFriends)
  get('https://randomuser.me/api/', drawUser)
}
