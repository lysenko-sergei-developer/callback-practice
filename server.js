'use strict'

const http = require('http');
// const staticServer = require('node-static');
// const file = new staticServer.Server('.');
const data = require('./server/data.js')

const express = require('express')
const app = express()

app.use(express.static(__dirname))
app.listen(3000)

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

app.get('/photos', (req, res) => {
  res.end(JSON.stringify(data.photos))
})

//
// http.createServer(function(req, res) {
//   if(req.url == '/photos') {
//     res.writeHead(200, {
//       'Content-Type': 'text/plain',
//       'Cache-Control': 'no-cahce'
//     })
//
//     res.end(JSON.stringify(data.photos))
//   } else {
//     file.serve(req, res);
//   }
// }).listen(3000);

console.log('Server running on port 3000');
